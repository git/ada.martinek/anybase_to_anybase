int	check(char *base)
{
	int	i;
	int	j;

	i = 0;
	while (base[i])
	{
		j = i + 1;
		while (base[j])
		{
			if (base[j] == base[i])
				return (0);
			j++;
		}
		if (base[i] == 43 || base[i] == 45)
			return (0);
		i++;
	}
	if (i < 2)
		return (0);
	return (i);
}

int	is_base(char k, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (k == base[i])
			return (1);
		i++;
	}
	return (0);
}

int	unchar(char k, char *base)
{
	int	i;

	i = 0;
	while (base[i])
	{
		if (k == base[i])
			return (i);
		i++;
	}
	return (0);
}

int	start(char *str)
{
	int	posi;
	int	i;

	i = 0;
	posi = 1;
	while (str[i] == 32 || (str[i] >= 9 && str[i] <= 13))
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			posi = posi * -1;
		i++;
	}
	return (posi);
}
