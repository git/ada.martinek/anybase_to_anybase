long	decode(char *nbr, char *base_from);
void	convert(long medium, char *base_to, char *unestring, int *p_i);
char	*jonemar(char *cepafini, char *celafin, int i);
char	*recode(long medium, char *base_to, int posi);
char	*convert_base(char *nbr, char *base_from, char *base_to);
int	check(char *base);
int	is_base(char k, char *base);
int	unchar(char k, char *base);
int	start(char *str);
