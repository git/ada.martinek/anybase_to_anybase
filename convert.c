#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "convert_base.h"

int	main(void)
{
	char	*nbr;
	char	*base_from;
	char	*base_to;

	nbr = (char *)malloc(33 * sizeof(char));
	base_from = (char *)malloc(50 * sizeof(char));
	base_to = (char *)malloc(50 * sizeof(char));
	if(!nbr || !base_from || !base_to)
	{
		write(2, "Erreur memoire, abandon", 24);
		return 1;
	}
	printf("Rentrez votre nombre\n");
	scanf("%32s", nbr);
	fflush(stdin);
	printf("Rentrez la base d'entrée de votre nombre\n");
	scanf("%49s", base_from);
	fflush(stdin);
	printf("Rentrez la base de sortie désirée\n");
	scanf("%49s", base_to);
	fflush(stdin);
	printf("%s\n", convert_base(nbr, base_from, base_to));
	free(nbr);
	free(base_from);
	free(base_to);
	return 0;
}
